article rédigé le 01/03/2021


Nous allons voir ici la création d'un container chez OVHcloud sur lequel nous autoriserons en lecture/écriture un utilisateur. Ce dernier n'aura aucun autre accès, ni aux autres containers du projet, ni aux autres ressources du projet.



## Constat et but de cet article


OVHCloud propose du stockage objets via OpenStak. Depuis quelques mois, on peut afiner les droits d'accés aux containers d'objets. De base, si vous passez par le manager pour créer un utilisteur, vous pourrez déjà avoir un premier niveau de gestion des droits, en limitant l'accés aux container.


![ovhcloud-choix-roles](uploads/ovhcloud-choix-roles.png)


Cependant, pour des raisons de sécurité et/ou de cloisonement, il peut être nécessaire de limiter l'accés à ces containers. **Le but étant de créer un utilisateur pour chaque container.** . Cet utilisateur aura uniquement accés à ce container.

Pour cela il y a deux méthodes :
* l'API d'OVHCloud
* les commandes Openstack



## Environnement de travail pour la première méthode

Pour présenter le concept, je vous propose de travailler avec l'OS Ubuntu Server 20.04 LTS via une VM créée par Vagrant, dans un Docker, une instance chez l'hébergeur de votre choix (les  instnaces S1-2 d'OVHcloud ou DEV1-S de Scaleway font plus que le nécessaire).

```bash
apt update
apt dist-upgrade
apt install php7.4-cli php-curl
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php composer-setup.php --install-dir=/usr/bin --filename=composer
composer require ovh/ovh
composer require php-opencloud/openstack ^3.0
composer update
```


## Création d'un utilisateur dédié via les API.


### Présentation des API utilisées

Les API sont présentées sur le site https://api.ovh.com/console/

![ovhcloud-api-storage-access](uploads/ovhcloud-api-storage-access.png)

Pour les tester, on peut se connecter et jouer avec les API directement depuis la page web.


Les API que nous allons utiliser sont :
* /cloud/project/{serviceName}/storage en POST pour créer un container
* /cloud/project/{serviceName}/storage en GET pour récupérer les informations d'un container (dans notre cas, ce sera l'id)
* /cloud/project/{serviceName}/storage/{idcontainer}/user en POST pour créer un utilisateur spécifique pour le container



### Utilisation des API

Pour pouvoir les utiliser via un script, il nous faut créer un tuple `application key`-`application-secret`-`consumer-key`. 

Soit depuis le manager d'OVHCloud, soit dans le fichier `openrc.sh` que vous pouvez récupérer là aussi dans le manager ou depuis https://horizon.cloud.ovh.net, vous devez noter l'`id` du projet.

Sur la manager, il est disponible sous le nom de votre projet Public Cloud.
Dans le fichier `openrc.sh`, l'id du projet est défini dans la variable `PROJECT_ID`.

Avec cette information, nous allons créer le tuple.

L'URL pour cela est :
https://api.ovh.com/createToken/index.cgi

![ovhcloud-creation-token](uploads/ovhcloud-creation-token.png)

Sinon, l'URL peut être construite ainsi :
https://api.ovh.com/createToken/index.cgi?GET=/cloud/project/PROJECT_ID/storage/*&POST=/cloud/project/PROJECT_ID/storage/*&GET=/cloud/project/PROJECT_ID/storage&POST=/cloud/project/PROJECT_ID/storage

Sur la page, vous pouvez limiter l'accés à ces API via le tuple que vous allez créer à une ou des IP publique définies. Le but étant de limiter l'impact en cas de compromission de ces credentials.


### Petit programme PHP pour utiliser ces API

Dans cet article, nous allons utiliser le SDK fourni par OVH.

https://github.com/ovh/php-ovh

En partant du principe que vous avez suivi les instructions d'installation via `composer`, nous allons créer une classe `OVHApplication.class.php`.

```php
<?php


class OVHApplication
{
    protected $api = null;
    protected $projectid;

    public function __construct()
    {
        $this->api = new \Ovh\Api(
        OVH_APPLICATION_KEY,
        OVH_APPLICATION_SECRET,
        OVH_END_POINT,
        OVH_CONSUMER_KEY
        );
        $this->projectid = OVH_ID_PROJECT;
    }

    public function createStorage($containername,$region){
        $data=array('archive' => false,'containerName' => $containername , "region" =>  $region );
        $path=sprintf('/cloud/project/%s/storage',$this->projectid);
        try {
            $raw = $this->api->post($path, $data);
        } catch (GuzzleHttp\Exception\ClientException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            echo $responseBodyAsString;
        }

        return $raw;
    }

    public function deleteStorage($containerid){
        $path=sprintf('/cloud/project/%s/storage/%s',$this->projectid,$containerid);
        try {
            $raw = $this->api->delete($path);
        } catch (GuzzleHttp\Exception\ClientException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            echo $responseBodyAsString;
        }
    }

    public function recupidstorage(){
        $path=sprintf('/cloud/project/%s/storage',$this->projectid);
        $raw = $this->api->get($path);
        return $raw;
    }

    public function createuserforcontainer($containerid,$description){
        $data=array('description'=>$description,'right' => 'all');
        $path=sprintf('/cloud/project/%s/storage/%s/user',$this->projectid,$containerid);
        try {
            $raw = $this->api->post($path,$data);
        } catch (GuzzleHttp\Exception\ClientException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            echo $responseBodyAsString;
        }

        return $raw;
    }

}
```

Cette classe permet de créer ou de supprimer un container (`createStorage` et `deleteStorage`). La méthode `createuserforcontainer` crée un utilisateur pour un container donné. Cet utilisateur aura les droits d'écriture et de lecture. 


Pour appeler cette classe, on peut utiliser un script comme celui-ci :
```php
<?php
require_once('./vendor/autoload.php');
require_once('./config.php');
require_once('./OVHApplication.class.php');

$containername=$argv[1];
$ovh = new OVHApplication();


$containerinfo = $ovh->createStorage($containername,OVH_REGION);
var_dump($containerinfo);
$idproject=$containerinfo['id'];


$user = $ovh->createuserforcontainer($idproject,$containername);
var_dump($user);
$fp = fopen('config-user.php', 'w');
fwrite($fp, "<?php\n");
fwrite($fp, "define('CONTAINER_USER_NAME','".$user['username']."');\n");
fwrite($fp, "define('CONTAINER_USER_PASSWORD','".$user['password']."');\n");
fwrite($fp, "define('CONTAINER_NAME','".$containername."');\n");
fclose($fp);
```

Pour la gestion des secrets, nous allons les renseigner dans un fichier à part, `config.php` :
```php
<?php
if ( ! defined("OVH_APPLICATION_KEY")) {
    define("OVH_APPLICATION_KEY", "XXXXX");
}

if ( ! defined("OVH_APPLICATION_SECRET")) {
    define("OVH_APPLICATION_SECRET", "YYYYYY");
}

if ( ! defined("OVH_CONSUMER_KEY")) {
    define("OVH_CONSUMER_KEY", "ZZZZZZZZ");
}

if ( ! defined("OVH_END_POINT")) {
    define("OVH_END_POINT", "ovh-eu");
}

// openrc.sh => OS_TENANT_ID
if ( ! defined("OVH_ID_PROJECT")){
    define('OVH_ID_PROJECT','123456789azerty');
}

if (! defined('REGIONSLIST')){
    define('REGIOSNLIST',array('SBG','GRA'));
}

// openrc.sh => OS_REGION_NAME
if (! defined('OVH_REGION')){
    define('OVH_REGION','SBG');
}

if (! defined('OVH_AUTH_URL')){
    define('OVH_AUTH_URL','https://auth.cloud.ovh.net/v3/');
}
```

Une fois les informations renseignées, nous allons pouvoir créer un container et un utilisateur uniquement pour celui-ci avec les droits de lecture et d'écriture.

```php
php index.php containertest
```

Je vous laisse voir les informations en sortie et depuis votre manager à la fois dans la partie "Object Storage" et "Users & Roles".


### Essayons d'envoyer un fichier

Comme vous l'avez remarqué, à la fin du fichier `index.php`, nous stockons certaines informations.
Elles nous servent pour nous authentifier sur l'Openstack afin d'ajouter un objet.
Créons le fichier `testupload.php` (qui est un copier coller de https://gist.github.com/maximevalette/0958ad9fd2ff1c2747731459ec34ba8c)
```php
<?php
require_once('./vendor/autoload.php');
require_once('./config-user.php');
require_once('./config.php');

use OpenCloud\OpenStack;
$client = new \GuzzleHttp\Client(['base_uri' => 'https://auth.cloud.ovh.net/v3/']);
$token = new \OpenStack\Identity\v3\Models\Token($client, new \OpenStack\Identity\v3\Api());
$token->create([
  'user' => [
    'name' => CONTAINER_USER_NAME,
    'domain' => [
      'name' => 'Default',
    ],
    'password' => CONTAINER_USER_PASSWORD,
  ],
]);

file_put_contents('openstack.json', json_encode($token->export()));

$cachedToken = json_decode(file_get_contents('openstack.json'), true);

$openstack = new \OpenStack\OpenStack([
  'authUrl' => CONTAINER_USER_NAME,
  'region' => OVH_REGION,
  'tenantName' => OVH_ID_PROJECT,
  'cachedToken' => $cachedToken,
]);

$service = $openstack->objectStoreV1();
$container = $service->getContainer(CONTAINER_NAME);

$container->createObject([
  'name' => 'index.php',
  'content' => 'Description index',
]);
```

Vous pouvez lancer le script (il faut attendre une ou deux minutes le temps que les utilisateurs soient créés et déployés) :
```bash
php testupload.php
```

Depuis, le manager d'OVHcloud vous verrez votre premier objet, index.php :blush:

## Permettre l'accès à un container via les API Openstack

### Création d'un utilisateur

Depuis le manager d'OVH, nous devons créer un utilisateur sans aucun droit.
Nous lui donnerons le nom "usersansdroit" et aucune case doit être cochée.

Pour la suite la démonstration, je vous invite à télécharger le fichier openrc.sh de cet utilisateur, en l'enregistrant avec le nom "usersansdroit.sh".

### Création d'un container

Toujours via le manager, nous créeons un container de type "privée" dans "Object Storage" avec pour nom "containerperso".


### Jouons avec les API d'Openstack

Pour cea il nous faut charger notre fichier openrc.sh de notre compte ayant un rôle d'administrateur. Si vous n'en avez pas, il faut vous créer un utilisateur avec le rôle administrateur dans votre projet Public Cloud dans "Users & Roles". Le fichier se télécharge via l'icone à droite de votre utilisateur.

Une fois enregistré sur votre poste :
```bash
sudo apt install python3-swiftclient
source openrc.sh
```

Nous allons nous vérifier la région. Il faut être dans la même que celle du container.
```bash
env | grep REGION
```

Au besoin, changer la region (`export OS_REGION_NAME=GRA` par exemple).

Nous listons les containers présents. Vous devriez voir le container précédement créé.
```bash
swift list
```

Désormais, regardons les droits sur celui-ci :
```bash
swift stat containerperso
```

Ce qui nous interesse ici ce sont les infos ` Read ACL` et `Write ACL` qui sont vides. Ce n'est pas parcequ'elles sont vides que personne ne peut lire ou écrire dans le container. Par exemple, avec votre compte admin vous pourriez ajouter des objets. Pour rendre, l'exercice plus parlant pour la suite,ajoutez un fichier dans votre container :
```bash
swift upload containerperso /chemin/vers/fichier
swift list containerperso
```

Nous allons ajouter les droits au fur et à mesure à l'utilisateur "usersansdroit".
Afin de voir les avant après, nous ouvrons un second terminal et chargeons les variables contenu dans dans `usersansdroit.sh`. (pensez à vérifier la REGION)

```bash
source usersansdroit.sh
```

Nous vérifions que nous ne pouvons pas lister les containers, ni voir les détails du container `containerperso` ou encore lister son contenu :
```bash
swift list
swift stat containerperso
swift list containerperso
swift upload containerperso /chemin/vers/unautrefichier
```

A présent que vous avons eu que des erreurs, ajoutons le droit de lecture à "usersansdroit" sur le container "containerperso", depuis notre terminal d'administrateur.
(remplacerusernameici correspond à la variable OS_USERNAME dans le fichier usersansdroit.sh)
```bash
swift post --read-acl "${OS_PROJECT_ID}:remplacerusernameici" containerperso
```


Dans le terminal de "usersansdroit", vous pouvez maintenant lister les objets présents dans le container mais toujours pas en ajouter ou voir les containers du projet :
```bash
swift list
swift stat containerperso
swift list containerperso
swift upload containerperso /chemin/vers/uautrefichier
```
Vous aurez remarqué, vous avez droit à des infos sur le container via la commande `swift stat`.


Dans le terminal d'administrateur, donnons le droit d'écriture :
```bash
swift post --write-acl "${OS_PROJECT_ID}:remplacerusernameici" containerperso
```

Essayons depuis le terminal "usersansdroit" d'ajouter un objet :
```bash
swift upload containerperso /chemin/vers/uautrefichier
swift list containerperso
```

Vous aurez peut être un warning, le fichier aura quand même été déposé dans le container.



Si vous vous êtez trompé sur un droit, vous pouvez remettre à zéro ainsi :
```bash
swift post --read-acl "" containerperso
swift post --write-acl "" containerperso
```

Les changements de droits en read et write peuvent se faire en une seule commande :
```bash
swift post --write-acl "${OS_PROJECT_ID}:remplacerusernameici" --read-acl "${OS_PROJECT_ID}:remplacerusernameici" containerperso
```
mais la démonstration aurai eu moins d'effet :sunglasses:


### Ajouter des droits à ceux existants

Dans le cas où l'on souhaite ajouter d'autres utilisateur, il faut concatainer avec les droits existants.
```bash
swift stat containerperso
```

Puis :

```bash
swift post --write-acl "reprendreexistant,${OS_PROJECT_ID}:remplacerusernameici" --read-acl "reprendreexistant,${OS_PROJECT_ID}:remplacerusernameici" containerperso
```



## Conclusion 

Cet article a permi de poser les bases pour une suite, au moins deux articles suivront, où nous verrons comment convertir l'accès au container avec les API S3. Nous terminerons avec l'envoie de gros fichiers et le mode "multi-part".



Merci à Rémi et Cécile pour leurs relectures et leurs contributions.
